package dam.android.batoipop.batoipop;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;

import dam.android.batoipop.batoipop.adapters.MyAdapterChat;
import dam.android.batoipop.batoipop.model.Chat;
import dam.android.batoipop.batoipop.model.Mensaje;

public class Chats extends AppCompatActivity {

    Button btLogout;
    RecyclerView rvMensajes;
    MyAdapterChat myAdapterChat;
    public static ArrayList<Chat> chats = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chats);
        initArray();
        rvMensajes = findViewById(R.id.rvMensajes);
        myAdapterChat = new MyAdapterChat(chats);
        rvMensajes.setLayoutManager(new LinearLayoutManager(this));
        rvMensajes.setAdapter(myAdapterChat);
        btLogout = findViewById(R.id.btLogout);
        btLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.logout();
                startActivity(new Intent(Chats.this, MainActivity.class));
                finish();
            }
        });
    }
    private void initArray() {
        ArrayList<Mensaje> mensajes1 = new ArrayList<>();
        ArrayList<Mensaje> mensajes2 = new ArrayList<>();
        ArrayList<Mensaje> mensajes3 = new ArrayList<>();
        ArrayList<Mensaje> mensajes4 = new ArrayList<>();
        Mensaje m1 = new Mensaje("Hola, me interesa el articulo, podrias rebajarlo?", 1);
        Mensaje m2 = new Mensaje("No, el precio es el que es", 5);
        Mensaje m3 = new Mensaje("Ok", 1);
        mensajes1.add(m1);
        mensajes1.add(m2);
        mensajes1.add(m3);
        Mensaje m4 = new Mensaje("Hola, me interesa el articulo, podrias rebajarlo?", 3);
        Mensaje m5 = new Mensaje("Te descuento 5€", 5);
        Mensaje m6 = new Mensaje("Perfecto", 3);
        mensajes2.add(m4);
        mensajes2.add(m5);
        mensajes2.add(m6);
        Mensaje m7 = new Mensaje("Eso es un poco peligroso", 5);
        Mensaje m8 = new Mensaje("Y?", 4);
        Mensaje m9 = new Mensaje("Que no deberias subir eso...", 5);
        Mensaje m10 = new Mensaje("Y?", 4);
        mensajes3.add(m7);
        mensajes3.add(m8);
        mensajes3.add(m9);
        mensajes3.add(m10);
        Mensaje m11 = new Mensaje("Un poco caro no?", 5);
        Mensaje m12 = new Mensaje("Es lo que hay... o lo tomas o lo dejas", 1);
        mensajes4.add(m11);
        mensajes4.add(m12);
        Chat chat1 = new Chat(1, 1, mensajes1);
        Chat chat2 = new Chat(2, 5, mensajes2);
        Chat chat3 = new Chat(3, 4, mensajes3);
        Chat chat4 = new Chat(4, 2, mensajes4);
        chats.clear();
        chats.add(chat1);
        chats.add(chat2);
        chats.add(chat3);
        chats.add(chat4);
    }
    public void menuToolbarClick(View v) {
        switch (v.getId()) {
            case R.id.btHome:
                MainActivity.articulosActual.clear();
                startActivity(new Intent(this, MainActivity.class));
                finish();
                break;
            case R.id.btCategories:
                startActivity(new Intent(this, Categorias.class));
                finish();
                break;
            case R.id.btAdd:
                if (MainActivity.actualUser != null) {
                    startActivity(new Intent(this, UploadArticle.class));
                } else {
                    Toast.makeText(this, "Logueate para usar esta funciòn", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(this, Login.class));
                }
                break;
            case R.id.btMsg:
                if (MainActivity.actualUser != null) {
                    startActivity(new Intent(this, Chats.class));
                    finish();
                } else {
                    Toast.makeText(this, "Logueate para usar esta funciòn", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(this, Login.class));
                }
                break;
            case R.id.btProfile:
                if (MainActivity.actualUser != null) {
                    startActivity(new Intent(this, UserProfile.class));
                    finish();
                } else {
                    Toast.makeText(this, "Logueate para usar esta funciòn", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(this, Login.class));
                }
                break;
        }
    }
}