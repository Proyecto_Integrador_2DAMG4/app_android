package dam.android.batoipop.batoipop.model;

import android.graphics.Bitmap;

public class Articulo {
   private int id, visitas, likes, usuario;
   private float precio;
   private String name, descripcion, rating, categoria;
   private Bitmap img;
   private Boolean liked;

   public Articulo(int id, String name, float precio, String categoria, String descripcion, int usuario, Bitmap img) {
      this.id = id;
      this.precio = precio;
      this.name = name;
      this.categoria = categoria;
      this.descripcion = descripcion;
      this.usuario = usuario;
      this.img = img;
      this.visitas = 0;
      this.likes = 0;
      this.liked = false;
   }

   public Articulo(String name, float precio, String categoria, String descripcion, int usuario, Bitmap img) {
      this.precio = precio;
      this.name = name;
      this.categoria = categoria;
      this.descripcion = descripcion;
      this.usuario = usuario;
      this.img = img;
      this.visitas = 0;
      this.likes = 0;
      this.liked = false;
   }

   public Boolean getLiked() {
      return liked;
   }

   public void setLiked(Boolean liked) {
      this.liked = liked;
   }

   public int getId() {
      return id;
   }

   public void setId(int id) {
      this.id = id;
   }

   public int getVisitas() {
      return visitas;
   }

   public void setVisitas(int visitas) {
      this.visitas = visitas;
   }

   public int getLikes() {
      return likes;
   }

   public void setLikes(int likes) {
      this.likes = likes;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public float getPrecio() {
      return precio;
   }

   public void setPrecio(float precio) {
      this.precio = precio;
   }

   public String getDescripcion() {
      return descripcion;
   }

   public void setDescripcion(String descripcion) {
      this.descripcion = descripcion;
   }

   public int getUsuario() {
      return usuario;
   }

   public void setUsuario(int usuario) {
      this.usuario = usuario;
   }

   public String getRating() {
      return rating;
   }

   public void setRating(String rating) {
      this.rating = rating;
   }

   public Bitmap getImg() {
      return img;
   }

   public void setImg(Bitmap img) {
      this.img = img;
   }

   public String getCategoria() {
      return categoria;
   }
}
