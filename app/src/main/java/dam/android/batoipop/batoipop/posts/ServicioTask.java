package dam.android.batoipop.batoipop.posts;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import dam.android.batoipop.batoipop.model.Articulo;
import dam.android.batoipop.batoipop.model.User;

/**
 * Created by Eddy on 14/01/2018.
 */

public class ServicioTask extends AsyncTask<Void, Void, String> {
   //variables del hilo
   private Context httpContext;//contexto
   ProgressDialog progressDialog;//dialogo cargando
   public String resultadoapi="";
   public String linkrequestAPI="";//link  para consumir el servicio rest
   public User user;
   public Articulo articulo;
   //constructor del hilo (Asynctask)
   public ServicioTask(Context ctx, String linkAPI, User user){
      this.httpContext=ctx;
      this.linkrequestAPI=linkAPI;
      this.user = user;
   }
   public ServicioTask(Context ctx, String linkAPI, Articulo articulo){
      this.httpContext=ctx;
      this.linkrequestAPI=linkAPI;
      this.articulo = articulo;
   }
   @Override
   protected void onPreExecute() {
      super.onPreExecute();
      progressDialog = ProgressDialog.show(httpContext, "Procesando Solicitud", "por favor, espere");
   }

   @Override
   protected String doInBackground(Void... params) {
      String result= null;

      String wsURL = linkrequestAPI;
      URL url = null;
      try {
         url = new URL(wsURL);
         HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
         //crear el objeto json para enviar por POST
         Gson gson = new Gson();
         String objetoJson = "";
         if (user != null) {
            objetoJson = gson.toJson(user, user.getClass());
            objetoJson = objetoJson.replace("\"id\":0,", "");
         } else {
            objetoJson = gson.toJson(articulo, articulo.getClass());
            objetoJson = objetoJson.replace("\"id\":0,", "");
         }


         //DEFINIR PARAMETROS DE CONEXION
         urlConnection.setReadTimeout(15000 /* milliseconds */);
         urlConnection.setConnectTimeout(15000 /* milliseconds */);
         urlConnection.setRequestMethod("POST");// se puede cambiar por delete ,put ,etc
         urlConnection.setRequestProperty("Content-Type", "application/json");
         urlConnection.setDoInput(true);
         urlConnection.setDoOutput(true);

         //OBTENER EL RESULTADO DEL REQUEST
         OutputStream os = urlConnection.getOutputStream();
         BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
         writer.write(objetoJson);
         writer.flush();
         writer.close();
         os.close();

         int responseCode=urlConnection.getResponseCode();// conexion OK?
         if(responseCode== HttpURLConnection.HTTP_OK){
            BufferedReader in= new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

            StringBuffer sb= new StringBuffer("");
            String linea="";
            while ((linea=in.readLine())!= null){
               sb.append(linea);
               break;
            }
            in.close();
            result= sb.toString();
         }
         else{
            result = "Error: " + responseCode;


         }


      } catch (MalformedURLException e) {
         e.printStackTrace();
      } catch (IOException e) {
         e.printStackTrace();
      } catch (Exception e) {
         e.printStackTrace();
      }


      return  result;

   }

   @Override
   protected void onPostExecute(String s) {
      super.onPostExecute(s);
      progressDialog.dismiss();
      resultadoapi=s;
      Toast.makeText(httpContext,resultadoapi,Toast.LENGTH_LONG).show();//mostrara una notificacion con el resultado del request

   }

}
