package dam.android.batoipop.batoipop.model;

public class Mensaje {
   private String mensaje;
   private int idUser;

   public Mensaje(String mensaje, int idUser) {
      this.mensaje = mensaje;
      this.idUser = idUser;
   }

   public String getMensaje() {
      return mensaje;
   }

   public void setMensaje(String mensaje) {
      this.mensaje = mensaje;
   }

   public int getIdUser() {
      return idUser;
   }

   public void setIdUser(int idUser) {
      this.idUser = idUser;
   }
}
