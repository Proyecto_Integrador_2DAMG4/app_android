package dam.android.batoipop.batoipop.model;

import java.util.ArrayList;

public class Chat {
   private int chatID;
   private int article;
   private ArrayList<Mensaje> mensajes;

   public Chat(int chatID, int article, ArrayList<Mensaje> mensajes) {
      this.chatID = chatID;
      this.article = article;
      this.mensajes = mensajes;
   }

   public Chat(int article, ArrayList<Mensaje> mensajes) {
      this.article = article;
      this.mensajes = mensajes;
   }

   public int getChatID() {
      return chatID;
   }

   public void setChatID(int chatID) {
      this.chatID = chatID;
   }

   public int getArticle() {
      return article;
   }

   public void setArticle(int article) {
      this.article = article;
   }

   public ArrayList<Mensaje> getMensajes() {
      return mensajes;
   }

   public void setMensajes(ArrayList<Mensaje> mensajes) {
      this.mensajes = mensajes;
   }
}
