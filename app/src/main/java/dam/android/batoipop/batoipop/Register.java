package dam.android.batoipop.batoipop;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Calendar;

import dam.android.batoipop.batoipop.model.User;
import dam.android.batoipop.batoipop.posts.ServicioTask;

public class Register extends AppCompatActivity {
    EditText etPersonName, etDNI, etMail, etBiDate,
            etAddress, etPhone, etPassword, etPassword2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        setUI();
    }

    private void setUI() {
        etPersonName = findViewById(R.id.etPersonName);
        etDNI = findViewById(R.id.etDNI);
        etMail = findViewById(R.id.etMail);
        etBiDate = findViewById(R.id.etBiDate);
        etAddress = findViewById(R.id.etAddress);
        etPhone = findViewById(R.id.etPhone);
        etPassword = findViewById(R.id.etPassword);
        etPassword2 = findViewById(R.id.etPassword2);
    }

    public void registration(View v) {
        if (etPassword.getText().toString().equals(etPassword2.getText().toString())) {
            User user = new User(etPersonName.getText().toString(),
                    etPhone.getText().toString(), etDNI.getText().toString(),
                    etMail.getText().toString(), etPassword.getText().toString(),
                    etAddress.getText().toString(), etBiDate.getText().toString());
            //metodoPOST.upload(user);
            ServicioTask servicioTask = new ServicioTask(this, "http://137.74.226.44:8080/usuarios/", user);
            servicioTask.execute();
            finish();
        } else
            Toast.makeText(this, "Los campos contraseña no coinciden", Toast.LENGTH_SHORT).show();


    }

    public void pickDate(View view) {
        int day, month, year;
        if (etBiDate.length() > 1) {
            day = Integer.parseInt(etBiDate.getText().toString().split("-")[2]);
            month = Integer.parseInt(etBiDate.getText().toString().split("-")[1]) - 1;
            year = Integer.parseInt(etBiDate.getText().toString().split("-")[0]);
        } else {
            final Calendar c = Calendar.getInstance();
            year = c.get(Calendar.YEAR);
            month = c.get(Calendar.MONTH);
            day = c.get(Calendar.DAY_OF_MONTH);
        }

        DatePickerDialog dpd = new DatePickerDialog(Register.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int pYear, int pMonth, int pDay) {
                StringBuilder sb = new StringBuilder();
                if (pMonth >= 9) sb.append(pYear).append("-").append(pMonth + 1);
                else sb.append(pYear).append("-").append("0").append(pMonth + 1);
                if (pDay >= 10) sb.append("-").append(pDay);
                else sb.append("-").append("0").append(pDay);
                etBiDate.setText(sb.toString());
            }
        }, year, month, day);
        dpd.show();
    }
}