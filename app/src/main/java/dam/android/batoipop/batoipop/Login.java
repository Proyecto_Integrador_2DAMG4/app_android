package dam.android.batoipop.batoipop;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import dam.android.batoipop.batoipop.model.User;

public class Login extends AppCompatActivity {

    EditText etMail, etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        etMail = findViewById(R.id.etMail);
        etPassword = findViewById(R.id.etPassword);
    }

    public void login(View v) {
        if (!etMail.getText().toString().equals("") && !etPassword.getText().toString().equals("")) {
            if (checkUser()) {
                if (checkPassword()) {
                    Toast.makeText(this, "Login correcto", Toast.LENGTH_SHORT).show();
                    finish();
                } else
                    Toast.makeText(this, "¡Contraseña incorrecta!", Toast.LENGTH_SHORT).show();
            } else
                Toast.makeText(this, "No existe el usuario", Toast.LENGTH_SHORT).show();
        }else
            Toast.makeText(this, "Rellena ambos campos", Toast.LENGTH_SHORT).show();
    }

    public void register(View v){
        startActivity(new Intent(this, Register.class));
    }

    private Boolean checkUser() {
        boolean b = false;
        for (User u : MainActivity.usersDB) {
            if (u.getEmail().equals(etMail.getText().toString())) b = true;
        }
        return b;
    }

    private Boolean checkPassword() {
        boolean b = false;
        for (User u : MainActivity.usersDB) {
            if (u.getEmail().equals(etMail.getText().toString()))
                if (u.getPassword().equals(etPassword.getText().toString())) {
                    b = true;
                    MainActivity.actualUser = u;
                }
        }
        return b;
    }
}