package dam.android.batoipop.batoipop.adapters;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import dam.android.batoipop.batoipop.MainActivity;
import dam.android.batoipop.batoipop.R;
import dam.android.batoipop.batoipop.model.Mensaje;

public class MyAdapterMsg extends RecyclerView.Adapter<MyAdapterMsg.MyViewHolder> {
        private ArrayList<Mensaje> myMsgList;

        // Class for each item
        static class MyViewHolder extends RecyclerView.ViewHolder {
            View view;
            TextView tvMsg;
            LinearLayout llMsg;

            public MyViewHolder(View view) {
                super(view);
                this.view = view;
                this.tvMsg = view.findViewById(R.id.tvMsg);
                this.llMsg = view.findViewById(R.id.llMsg);
            }

            // sets viewHolder views with data
            public void bind(Mensaje mensaje) {
                this.tvMsg.setText(mensaje.getMensaje());
                if (mensaje.getIdUser() == MainActivity.actualUser.getId()) {
                    llMsg.setGravity(Gravity.END);
                } else {
                    llMsg.setGravity(Gravity.START);
                }

            }
        }


    // constructor: todoListDBManager gets DB data
        public MyAdapterMsg(ArrayList<Mensaje> myMsgList) {
            getData(myMsgList);
        }

        // get data from DB
        public void getData(ArrayList<Mensaje> myMsgList) {
            this.myMsgList = myMsgList;
            notifyDataSetChanged();
        }

        // Creates new view item: Layout Manager calls this method
        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            // Create item View:
            View chatLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.mensaje_layout, parent, false);
            return new MyViewHolder(chatLayout);
        }

        // replaces the data content of a viewholder (recycles old viewHolder): Layout Manager calls this method
        @Override
        public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
            // bind viewHolder with data at: position
            viewHolder.bind(myMsgList.get(position));
        }

        // returns the size of dataSet: Layout Manager calls this method
        @Override
        public int getItemCount() { return myMsgList.size(); }


}