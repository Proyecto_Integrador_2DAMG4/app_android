package dam.android.batoipop.batoipop.posts;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import com.google.gson.Gson;

import dam.android.batoipop.batoipop.model.User;

/**
 * Hello world!
 *
 */
public class metodoPOST {

	private User user;

	public metodoPOST(User user) {
		this.user = user;
	}

	public static void upload(User user) {
		metodoPOST metodoPOST = new metodoPOST(user);
		metodoPOST.start();
	}
	private void start() {
		Gson gson;
		try {
			gson = new Gson();
			String clienteJson = gson.toJson(user, User.class);
			clienteJson = clienteJson.replace("\"id\":0,", "");
			System.out.println(clienteJson);
			enviaCliente(clienteJson);
			System.out.println("OK");
		} catch (RuntimeException e) {
			System.out.println("Peta aqui");
		}

	}

	private static void enviaCliente(String clienteJson) {	
		URL url;
		HttpURLConnection con;

		try {
			url = new URL("http://137.74.226.44:8080/usuarios/");
			con = (HttpURLConnection) url.openConnection();
			con.setDoOutput(true);
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json");

			OutputStream os = con.getOutputStream();
			os.write(clienteJson.getBytes());
			os.flush();
			
			if (con.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
				throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
			} 				
			con.disconnect();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

	
}
