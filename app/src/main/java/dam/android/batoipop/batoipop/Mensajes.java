package dam.android.batoipop.batoipop;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import dam.android.batoipop.batoipop.adapters.MyAdapterMsg;
import dam.android.batoipop.batoipop.model.Articulo;

public class Mensajes extends AppCompatActivity {

    Button btLogout;
    RecyclerView rvMensajes;
    MyAdapterMsg myAdapterMsg;
    ImageView imgUserChat;
    TextView tvUserChat;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mensajes);
        Bundle b = getIntent().getExtras();
        setUI(b);
    }
    private void setUI(Bundle b) {
        rvMensajes = findViewById(R.id.rvMsg);
        imgUserChat = findViewById(R.id.imgUserChat);
        tvUserChat = findViewById(R.id.tvUserArticleChat);
        myAdapterMsg = new MyAdapterMsg(Chats.chats.get(b.getInt("chat") - 1).getMensajes());
        rvMensajes.setLayoutManager(new LinearLayoutManager(this));
        rvMensajes.setAdapter(myAdapterMsg);
        Articulo a = getArt(Chats.chats.get(b.getInt("chat") - 1).getArticle());
        imgUserChat.setImageBitmap(a.getImg());
        tvUserChat.setText(a.getName());
        btLogout = findViewById(R.id.btLogout);
        btLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.logout();
                startActivity(new Intent(Mensajes.this, MainActivity.class));
                finish();
            }
        });
    }

    public Articulo getArt(int id) {
        Articulo art = null;
        for (Articulo a: MainActivity.articulosDB) {
            if (a.getId() == id) art = a;
        }
        return art;
    }

    public void menuToolbarClick(View v) {
        switch (v.getId()) {
            case R.id.btHome:
                MainActivity.articulosActual.clear();
                startActivity(new Intent(this, MainActivity.class));
                finish();
                break;
            case R.id.btCategories:
                startActivity(new Intent(this, Categorias.class));
                finish();
                break;
            case R.id.btAdd:
                if (MainActivity.actualUser != null) {
                    startActivity(new Intent(this, UploadArticle.class));
                } else {
                    Toast.makeText(this, "Logueate para usar esta funciòn", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(this, Login.class));
                }
                break;
            case R.id.btMsg:
                if (MainActivity.actualUser != null) {
                    startActivity(new Intent(this, Chats.class));
                    finish();
                } else {
                    Toast.makeText(this, "Logueate para usar esta funciòn", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(this, Login.class));
                }
                break;
            case R.id.btProfile:
                if (MainActivity.actualUser != null) {
                    startActivity(new Intent(this, UserProfile.class));
                    finish();
                } else {
                    Toast.makeText(this, "Logueate para usar esta funciòn", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(this, Login.class));
                }
                break;
        }
    }
}