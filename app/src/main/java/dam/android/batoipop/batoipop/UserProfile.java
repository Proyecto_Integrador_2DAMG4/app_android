package dam.android.batoipop.batoipop;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import dam.android.batoipop.batoipop.adapters.MyAdapterArt;
import dam.android.batoipop.batoipop.model.Articulo;


public class UserProfile extends AppCompatActivity {

    RecyclerView rvArticulos;
    GridLayoutManager glm;
    MyAdapterArt myAdapterArt;
    ArrayList<Articulo> articulos = new ArrayList<>();
    TextView tvUserName;
    Button btLogout;
    ImageView imgUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        SetUI();
    }

    private void SetUI() {
        rvArticulos = findViewById(R.id.rvUserArticles);
        for (Articulo a : MainActivity.articulosDB) {
            if (a.getUsuario() == MainActivity.actualUser.getId())
                articulos.add(a);
        }
        glm = new GridLayoutManager(this,2);
        rvArticulos.setLayoutManager(glm);
        myAdapterArt = new MyAdapterArt(articulos);
        rvArticulos.setAdapter(myAdapterArt);
        imgUser = findViewById(R.id.imgUser);
        imgUser.setImageBitmap(MainActivity.actualUser.getImage());
        tvUserName = findViewById(R.id.tvUser);
        tvUserName.setText(MainActivity.actualUser.getName());
        btLogout = findViewById(R.id.btLogout);
        btLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.logout();
                startActivity(new Intent(UserProfile.this, MainActivity.class));
                finish();
            }
        });
    }

    public void menuToolbarClick(View v) {
        switch (v.getId()) {
            case R.id.btHome:
                MainActivity.articulosActual.clear();
                MainActivity.search = false;
                startActivity(new Intent(this, MainActivity.class));
                finish();
                break;
            case R.id.btCategories:
                startActivity(new Intent(this, Categorias.class));
                finish();
                break;
            case R.id.btAdd:
                if (MainActivity.actualUser != null) {
                    startActivity(new Intent(this, UploadArticle.class));
                } else {
                    Toast.makeText(this, "Logueate para usar esta funciòn", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(this, Login.class));
                }
                break;
            case R.id.btMsg:
                if (MainActivity.actualUser != null) {
                    startActivity(new Intent(this, Chats.class));
                    finish();
                } else {
                    Toast.makeText(this, "Logueate para usar esta funciòn", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(this, Login.class));
                }
                break;
            case R.id.btProfile:
                if (MainActivity.actualUser != null) {
                    startActivity(new Intent(this, UserProfile.class));
                    finish();
                } else {
                    Toast.makeText(this, "Logueate para usar esta funciòn", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(this, Login.class));
                }
                break;
        }
    }

    public Bitmap generateBitmap(int id) {
        return BitmapFactory.decodeResource(this.getResources(), id);
    }
}