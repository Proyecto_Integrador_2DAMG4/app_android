package dam.android.batoipop.batoipop;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import dam.android.batoipop.batoipop.adapters.MyAdapterArt;
import dam.android.batoipop.batoipop.model.Articulo;
import dam.android.batoipop.batoipop.model.User;


public class MainActivity extends AppCompatActivity {

    RecyclerView rvArticulos;
    GridLayoutManager glm;
    MyAdapterArt myAdapterArt;
    Button btLogin;
    SearchView searchBar;
    ExecutorService executor;
    ProgressBar progressBar;
    TextView tvEmpty;
    public static ArrayList<Articulo> articulosDB = new ArrayList<>();
    public static ArrayList<Articulo> articulosActual = new ArrayList<>();
    public static ArrayList<User> usersDB = new ArrayList<>();
    public static User actualUser;
    public static boolean search = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SetUI();
    }

    private void SetUI() {
        Toolbar t = findViewById(R.id.toolbar);
        setSupportActionBar(t);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        progressBar = findViewById(R.id.progress_circular);
        rvArticulos = findViewById(R.id.rvArticulos);
        glm = new GridLayoutManager(this,2);
        rvArticulos.setLayoutManager(glm);
        myAdapterArt = new MyAdapterArt(articulosActual);
        rvArticulos.setAdapter(myAdapterArt);
        btLogin = findViewById(R.id.btLogin);
        if (actualUser != null) btLogin.setText(R.string.logout);
        else btLogin.setText(R.string.login);
        tvEmpty = findViewById(R.id.tvEmpty);
        loadUsers();
        if (search && articulosActual.size() == 0) tvEmpty.setVisibility(View.VISIBLE);
        else tvEmpty.setVisibility(View.INVISIBLE);
        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (btLogin.getText().equals("Login"))
                startActivity(new Intent(MainActivity.this, Login.class));
                if (btLogin.getText().equals("Logout")) {
                    logout();
                    startActivity(new Intent(MainActivity.this, MainActivity.class));
                    finish();
                }
            }
        });
        if (articulosActual.size() == 0 && !search) load();
        myAdapterArt.notifyDataSetChanged();
        searchBar = findViewById(R.id.searchBar);
        searchBar.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                MainActivity.articulosActual.clear();
                MainActivity.search = true;
                for (Articulo a : MainActivity.articulosDB) {
                    if (a.getName().contains(s))
                        MainActivity.articulosActual.add(a);
                }
                startActivity(new Intent(MainActivity.this, MainActivity.class));
                finish();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
    }

    public void menuToolbarClick(View v) {
        switch (v.getId()) {
            case R.id.btHome:
                articulosActual.clear();
                MainActivity.search = false;
                startActivity(new Intent(this, MainActivity.class));
                finish();
                break;
            case R.id.btCategories:
                startActivity(new Intent(this, Categorias.class));
                finish();
                break;
            case R.id.btAdd:
                if (actualUser != null) {
                    startActivity(new Intent(this, UploadArticle.class));
                } else {
                    Toast.makeText(this, "Logueate para usar esta funciòn", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(MainActivity.this, Login.class));
                }
                break;
            case R.id.btMsg:
                if (actualUser != null) {
                    startActivity(new Intent(this, Chats.class));
                    finish();
                } else {
                    Toast.makeText(this, "Logueate para usar esta funciòn", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(MainActivity.this, Login.class));
                }
                break;
            case R.id.btProfile:
                if (actualUser != null) {
                    startActivity(new Intent(this, UserProfile.class));
                    finish();
                } else {
                    Toast.makeText(this, "Logueate para usar esta funciòn", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(MainActivity.this, Login.class));
                }
                break;
        }
    }

    public static void logout(){
        actualUser = null;
    }
    private void startBackgroundTaskArt(URL url) {
        final int CONNECTION_TIMEOUT = 10000;
        final int READ_TIMEOUT = 7000;
        executor = Executors.newSingleThreadExecutor();
        Handler handler = new Handler(Looper.getMainLooper());
        executor.execute(new Runnable() {
            @Override
            public void run() {
                HttpURLConnection urlConnection = null;
                final ArrayList<Articulo> searchResult = new ArrayList<>();
                try {
                    // 1. Create connection object by invoking the openConnection method on a URL
                    urlConnection = (HttpURLConnection) url.openConnection();
                    // 2. Setup connection parameters
                    urlConnection.setConnectTimeout(CONNECTION_TIMEOUT);
                    urlConnection.setReadTimeout(READ_TIMEOUT);
                    // 3. connect to remote object (urlConnection)
                    urlConnection.connect();
                    // 4. The remote object becomes available.
                    // The header fields and the contents of the remote object can be accessed
                    getDataArt(urlConnection, searchResult);
                } catch (IOException e) {
                    Log.i("IOException", e.getMessage());
                    //searchResult.add("IOException: " + e.getMessage());
                } catch (JSONException e) {
                    Log.i("JSONException", e.getMessage());
                    //searchResult.add("JSONException: " + e.getMessage());
                } finally {
                    if (urlConnection != null) urlConnection.disconnect();
                }
                // Finally, update UI
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (searchResult.size() > 0) {
                            // set new results to list adapter
                            MyAdapterArt adapter = (MyAdapterArt) rvArticulos.getAdapter();
                            articulosDB.clear();
                            articulosDB.addAll(searchResult);
                            articulosActual.clear();
                            articulosActual.addAll(searchResult);
                            adapter.notifyDataSetChanged();
                        } else // use if possible ApplicationContext()
                            Toast.makeText(getApplicationContext(),
                                    "Not possible to contact BatoiPop", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
        setProgressBar(false);
    }

    private void getDataArt(HttpURLConnection urlConnection, ArrayList<Articulo> searchResult)
            throws IOException, JSONException {
        // Check if response was OK (response=200)
        if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
            //read data stream from url
            String resultStream = readStream(urlConnection.getInputStream());
            // create JSON object from result stream
            JSONArray jArray = new JSONArray(resultStream);
            if (jArray.length() > 0) {
                // fill list with data form summary attribute
                for (int i = 0; i < jArray.length(); i++) {
                    Bitmap bitmap = generateBitmap(R.mipmap.ic_launcher);
                    JSONObject item = jArray.getJSONObject(i);
                    InputStream is;
                    try {
                        is = new URL("http://137.74.226.44:8069/web/image/?model=batoi_pop.articulos&field=imagen&session_id=fecd4af087a926cdaec5af80612d78bb9acc4709&id="
                                + item.getInt("id")).openStream();
                        bitmap = BitmapFactory.decodeStream(is);
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }

                    Articulo a = new Articulo(item.getInt("id"),
                            item.getString("name"), Float.parseFloat(String.valueOf(item.getDouble("precio"))),
                            item.getJSONObject("batoiPopCategorias").getString("name"), item.getString("descripcion"),
                            item.getJSONObject("batoiPopUsuariosByUsuarios").getInt("id"), bitmap);
                    searchResult.add(a);
                }
            }
        } else {
            Log.i("URL", "ErrorCode: " + urlConnection.getResponseCode());
            //searchResult.add("HttpURLConnection ErrorCode: " + ur1Connection.getResponseCode());
        }
    }
    private String readStream(InputStream in) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8));
        String nextLine = "";
        while ((nextLine = reader.readLine()) != null) {
            sb.append(nextLine);
        }
        return sb.toString();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (executor != null ) {
            executor.shutdownNow(); Log.i("EXECUTOR","ALL TASKS CANCELLED !!!!!!");
        }
    }
    public void setProgressBar(boolean show) {
        if (show) progressBar.setVisibility(View.VISIBLE);
        else progressBar.setVisibility(View.INVISIBLE);
    }
    private void load() {
        setProgressBar(true);
        URL url;
        try {
            String s = "http://137.74.226.44:8080/articulos";
            url = new URL(s);
            startBackgroundTaskArt(url);
        } catch (MalformedURLException e) {
            Log.i("URL", e.getMessage());
        }
    }

    public void loadUsers() {
        URL url;
        try {
            String s = "http://137.74.226.44:8080/usuarios";
            url = new URL(s);
            startBackgroundTaskUser(url);
        } catch (MalformedURLException e) {
            Log.i("URL", e.getMessage());
        }
    }
    public Bitmap generateBitmap(int id) {
        return BitmapFactory.decodeResource(this.getResources(), id);
    }

    public static void UpdateLikes(int id, boolean f) {
        for (Articulo a : articulosActual) {
            if (a.getId() == id) {
                if (f) a.setLikes(a.getLikes() + 1);
                else a.setLikes(a.getLikes() - 1);
            }
        }

        for (Articulo a : articulosDB) {
            if (a.getId() == id) {
                if (f) a.setLikes(a.getLikes() + 1);
                else a.setLikes(a.getLikes() - 1);
            }
        }
    }

    private void startBackgroundTaskUser(URL url) {
        final int CONNECTION_TIMEOUT = 10000;
        final int READ_TIMEOUT = 7000;
        executor = Executors.newSingleThreadExecutor();
        Handler handler = new Handler(Looper.getMainLooper());
        executor.execute(new Runnable() {
            @Override
            public void run() {
                HttpURLConnection urlConnection = null;
                final ArrayList<User> searchResult = new ArrayList<>();
                try {
                    // 1. Create connection object by invoking the openConnection method on a URL
                    urlConnection = (HttpURLConnection) url.openConnection();
                    // 2. Setup connection parameters
                    urlConnection.setConnectTimeout(CONNECTION_TIMEOUT);
                    urlConnection.setReadTimeout(READ_TIMEOUT);
                    // 3. connect to remote object (urlConnection)
                    urlConnection.connect();
                    // 4. The remote object becomes available.
                    // The header fields and the contents of the remote object can be accessed
                    getDataUser(urlConnection, searchResult);
                } catch (IOException e) {
                    Log.i("IOException", e.getMessage());
                    //searchResult.add("IOException: " + e.getMessage());
                } catch (JSONException e) {
                    Log.i("JSONException", e.getMessage());
                    //searchResult.add("JSONException: " + e.getMessage());
                } finally {
                    if (urlConnection != null) urlConnection.disconnect();
                }
                // Finally, update UI
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (searchResult.size() > 0) {
                            usersDB.clear();
                            usersDB.addAll(searchResult);
                        } else // use if possible ApplicationContext()
                            Toast.makeText(getApplicationContext(),
                                    "Not possible to contact BatoiPop", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
        setProgressBar(false);
    }

    private void getDataUser(HttpURLConnection urlConnection, ArrayList<User> searchResult)
            throws IOException, JSONException {
        // Check if response was OK (response=200)
        if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
            //read data stream from url
            String resultStream = readStream(urlConnection.getInputStream());
            // create JSON object from result stream
            JSONArray jArray = new JSONArray(resultStream);
            if (jArray.length() > 0) {
                // fill list with data form summary attribute
                for (int i = 0; i < jArray.length(); i++) {
                    Bitmap bitmap = generateBitmap(R.mipmap.ic_launcher);
                    JSONObject item = jArray.getJSONObject(i);
                    InputStream is;
                    try {
                        is = new URL("http://137.74.226.44:8069/web/image/?model=batoi_pop.usuarios&field=imagen&session_id=fecd4af087a926cdaec5af80612d78bb9acc4709&id="
                                + item.getInt("id")).openStream();
                        bitmap = BitmapFactory.decodeStream(is);
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                    searchResult.add(new User(item.getInt("id"), item.getString("name"),
                            item.getString("telefono"), item.getString("dni"),
                            item.getString("email"), item.getString("password"),
                            item.getString("direccion"), item.getString("fechanac"), bitmap));
                }
            }
        } else {
            Log.i("URL", "ErrorCode: " + urlConnection.getResponseCode());
            //searchResult.add("HttpURLConnection ErrorCode: " + ur1Connection.getResponseCode());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (actualUser != null) btLogin.setText(R.string.logout);
        else btLogin.setText(R.string.login);
    }
}