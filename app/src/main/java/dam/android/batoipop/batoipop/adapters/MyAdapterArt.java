package dam.android.batoipop.batoipop.adapters;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import dam.android.batoipop.batoipop.ArticleBig;
import dam.android.batoipop.batoipop.R;
import dam.android.batoipop.batoipop.model.Articulo;

public class MyAdapterArt extends RecyclerView.Adapter<MyAdapterArt.MyViewHolder> {
        private ArrayList<Articulo> myArticleList;

        // Class for each item
        static class MyViewHolder extends RecyclerView.ViewHolder {
            View view;
            TextView tvName;
            TextView tvVisitas;
            TextView tvLikes;
            TextView tvPrecio;
            ImageView imgArticulo;
            ImageButton imbtLike;
            Boolean like;

            public MyViewHolder(View view) {
                super(view);
                this.view = view;
                this.tvName = view.findViewById(R.id.tvName);
                this.tvPrecio = view.findViewById(R.id.tvPrecio);
                this.tvVisitas = view.findViewById(R.id.tvVisitas);
                this.tvLikes = view.findViewById(R.id.tvLikes);
                this.imgArticulo = view.findViewById(R.id.imgArticulo);
                this.imbtLike = view.findViewById(R.id.imbtLike);
                like = false;
            }

            // sets viewHolder views with data
            public void bind(Articulo articulo) {
                this.tvName.setText(articulo.getName());
                this.tvVisitas.setText(String.valueOf(articulo.getVisitas()));
                this.tvLikes.setText(String.valueOf(articulo.getLikes()));
                this.tvPrecio.setText(articulo.getPrecio() + "€");
                this.imgArticulo.setImageBitmap(articulo.getImg());
                //this.imgArticulo.setImageResource(R.mipmap.ic_launcher);
                this.imbtLike.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!articulo.getLiked()) {
                            view.setBackgroundResource(android.R.drawable.btn_star_big_on);
                            Toast.makeText(MyViewHolder.super.itemView.getContext(), "like", Toast.LENGTH_SHORT).show();
                            articulo.setLikes(articulo.getLikes() + 1);
                            articulo.setLiked(true);

                        }
                        else {
                            view.setBackgroundResource(android.R.drawable.btn_star_big_off);
                            Toast.makeText(MyViewHolder.super.itemView.getContext(), "unlike", Toast.LENGTH_SHORT).show();
                            articulo.setLikes(articulo.getLikes() - 1);
                            articulo.setLiked(false);
                        }
                    }
                });
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Bundle b = new Bundle();
                        b.putInt("id", articulo.getId());
                        MyViewHolder.super.itemView.getContext().startActivity(new Intent(MyViewHolder.super.itemView.getContext(), ArticleBig.class).putExtras(b));
                    }
                });
            }

            public Bitmap generateBitmap(int id) {
                return BitmapFactory.decodeResource(MyViewHolder.super.itemView.getContext().getResources(), id);
            }
        }


    // constructor: todoListDBManager gets DB data
        public MyAdapterArt(ArrayList<Articulo> articulos) {
            getData(articulos);
        }

        // get data from DB
        public void getData(ArrayList<Articulo> articulos) {
            this.myArticleList = articulos;
            notifyDataSetChanged();
        }

        // Creates new view item: Layout Manager calls this method
        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            // Create item View:
            View articuloLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.articulo_layout, parent, false);
            return new MyViewHolder(articuloLayout);
        }

        // replaces the data content of a viewholder (recycles old viewHolder): Layout Manager calls this method
        @Override
        public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
            // bind viewHolder with data at: position
            viewHolder.bind(myArticleList.get(position));
        }

        // returns the size of dataSet: Layout Manager calls this method
        @Override
        public int getItemCount() { return myArticleList.size(); }


}