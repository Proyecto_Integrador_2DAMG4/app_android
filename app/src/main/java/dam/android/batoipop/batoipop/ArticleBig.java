package dam.android.batoipop.batoipop;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import dam.android.batoipop.batoipop.model.Articulo;

public class ArticleBig extends AppCompatActivity {

    ImageButton imbtLike;
    ArrayList<Articulo> articulos = new ArrayList<>();
    Articulo actual;
    TextView tvName;
    TextView tvVisitas;
    TextView tvLikes;
    TextView tvPrecio;
    TextView tvDescripcion;
    ImageView imgArticulo;
    Button btLogin;
    SearchView searchBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_big);
        Bundle b = getIntent().getExtras();
        setUI(b);
    }

    private void setUI(Bundle b) {
        imbtLike = findViewById(R.id.imbtLike);
        articulos = MainActivity.articulosDB;
        actual = findArticle(b.getInt("id"));
        tvName = findViewById(R.id.tvNameBig);
        tvPrecio = findViewById(R.id.tvPrecioBig);
        tvDescripcion = findViewById(R.id.tvDescripcion);
        tvLikes = findViewById(R.id.tvLikesBig);
        tvVisitas = findViewById(R.id.tvVisitasBig);
        imgArticulo = findViewById(R.id.imgArticle);
        btLogin = findViewById(R.id.btLogin);
        if (MainActivity.actualUser != null) btLogin.setText(R.string.logout);
        else btLogin.setText(R.string.login);
        if (actual.getLiked()) imbtLike.setBackgroundResource(android.R.drawable.btn_star_big_on);
        setData();
        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (btLogin.getText().equals("Login"))
                    startActivity(new Intent(ArticleBig.this, Login.class));
                if (btLogin.getText().equals("Logout")) {
                    MainActivity.logout();
                    startActivity(new Intent(ArticleBig.this, MainActivity.class));
                    finish();
                }
            }
        });
        searchBar = findViewById(R.id.searchBar);
        searchBar.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                MainActivity.articulosActual.clear();
                MainActivity.search = true;
                for (Articulo a : MainActivity.articulosDB) {
                    if (a.getName().contains(s))
                        MainActivity.articulosActual.add(a);
                }
                startActivity(new Intent(ArticleBig.this, MainActivity.class));
                finish();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
    }
    public void menuToolbarClick(View v) {
        switch (v.getId()) {
            case R.id.btHome:
                MainActivity.articulosActual.clear();
                MainActivity.search = false;
                startActivity(new Intent(this, MainActivity.class));
                finish();
                break;
            case R.id.btCategories:
                startActivity(new Intent(this, Categorias.class));
                finish();
                break;
            case R.id.btAdd:
                if (MainActivity.actualUser != null) {
                    startActivity(new Intent(this, UploadArticle.class));
                } else {
                    Toast.makeText(this, "Logueate para usar esta funciòn", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(this, Login.class));
                }
                break;
            case R.id.btMsg:
                if (MainActivity.actualUser != null) {
                    startActivity(new Intent(this, Chats.class));
                    finish();
                } else {
                    Toast.makeText(this, "Logueate para usar esta funciòn", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(this, Login.class));
                }
                break;
            case R.id.btProfile:
                if (MainActivity.actualUser != null) {
                    startActivity(new Intent(this, UserProfile.class));
                    finish();
                } else {
                    Toast.makeText(this, "Logueate para usar esta funciòn", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(this, Login.class));
                }
                break;
        }
    }

    private void setData() {
        if (actual.getImg() != null) imgArticulo.setImageBitmap(actual.getImg());
        else imgArticulo.setImageResource(R.mipmap.ic_launcher);
        tvPrecio.setText(actual.getPrecio() + "€");
        tvName.setText(actual.getName());
        tvDescripcion.setText(actual.getDescripcion());
        tvVisitas.setText(String.valueOf(actual.getVisitas()));
        tvLikes.setText(String.valueOf(actual.getLikes()));
    }


    private Articulo findArticle(int id) {
        Articulo art = null;
        for (Articulo a : articulos) {
            if (a.getId() == id) art = a;
        }
        return art;
    }

    public void Like(View v) {
        if (!actual.getLiked()) {
            v.setBackgroundResource(android.R.drawable.btn_star_big_on);
            Toast.makeText(this, "like", Toast.LENGTH_SHORT).show();
            actual.setLikes(actual.getLikes() + 1);
            actual.setLiked(true);
            MainActivity.UpdateLikes(actual.getId(), true);
        }
        else {
            v.setBackgroundResource(android.R.drawable.btn_star_big_off);
            Toast.makeText(this, "unlike", Toast.LENGTH_SHORT).show();
            actual.setLikes(actual.getLikes() - 1);
            actual.setLiked(false);
            MainActivity.UpdateLikes(actual.getId(), false);
        }
    }


}