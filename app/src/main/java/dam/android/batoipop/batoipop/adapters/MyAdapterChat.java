package dam.android.batoipop.batoipop.adapters;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import dam.android.batoipop.batoipop.MainActivity;
import dam.android.batoipop.batoipop.Mensajes;
import dam.android.batoipop.batoipop.R;
import dam.android.batoipop.batoipop.model.Articulo;
import dam.android.batoipop.batoipop.model.Chat;

public class MyAdapterChat extends RecyclerView.Adapter<MyAdapterChat.MyViewHolder> {
        private ArrayList<Chat> myChatList;

        // Class for each item
        static class MyViewHolder extends RecyclerView.ViewHolder {
            View view;
            TextView tvUserArticleChat;
            TextView tvLastMsg;
            ImageView imgUserChat;
            Articulo art;

            public MyViewHolder(View view) {
                super(view);
                this.view = view;
                this.tvUserArticleChat = view.findViewById(R.id.tvUserArticleChat);
                this.tvLastMsg = view.findViewById(R.id.tvLastMsg);
                this.imgUserChat = view.findViewById(R.id.imgUserChat);
            }

            // sets viewHolder views with data
            public void bind(Chat chat) {
                art = getArt(chat.getArticle());
                this.tvUserArticleChat.setText(art.getName());
                this.tvLastMsg.setText(String.valueOf(chat.getMensajes().get(chat.getMensajes().size() - 1).getMensaje()));
                this.imgUserChat.setImageBitmap(art.getImg());
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Bundle b = new Bundle();
                        b.putInt("chat", chat.getChatID());
                        MyViewHolder.super.itemView.getContext().startActivity(new Intent(MyViewHolder.super.itemView.getContext(), Mensajes.class).putExtras(b));
                    }
                });
            }

            public Articulo getArt(int id) {
                Articulo art = null;
                for (Articulo a: MainActivity.articulosDB) {
                    if (a.getId() == id) art = a;
                }
                return art;
            }
        }


    // constructor: todoListDBManager gets DB data
        public MyAdapterChat(ArrayList<Chat> chats) {
            getData(chats);
        }

        // get data from DB
        public void getData(ArrayList<Chat> chats) {
            this.myChatList = chats;
            notifyDataSetChanged();
        }

        // Creates new view item: Layout Manager calls this method
        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            // Create item View:
            View chatLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_card, parent, false);
            return new MyViewHolder(chatLayout);
        }

        // replaces the data content of a viewholder (recycles old viewHolder): Layout Manager calls this method
        @Override
        public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
            // bind viewHolder with data at: position
            viewHolder.bind(myChatList.get(position));
        }

        // returns the size of dataSet: Layout Manager calls this method
        @Override
        public int getItemCount() { return myChatList.size(); }


}