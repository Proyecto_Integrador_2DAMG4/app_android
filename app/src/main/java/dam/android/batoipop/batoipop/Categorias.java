package dam.android.batoipop.batoipop;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import dam.android.batoipop.batoipop.model.Articulo;

public class Categorias extends AppCompatActivity {

    Button btLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categorias);
        btLogin = findViewById(R.id.btLogin);
        if (MainActivity.actualUser != null) btLogin.setText(R.string.logout);
        else btLogin.setText(R.string.login);
        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (btLogin.getText().equals("Login"))
                    startActivity(new Intent(Categorias.this, Login.class));
                if (btLogin.getText().equals("Logout")) {
                    MainActivity.logout();
                    startActivity(new Intent(Categorias.this, MainActivity.class));
                    finish();
                }
            }
        });
    }

    public void onSelect(View v) {
        MainActivity.articulosActual.clear();
        MainActivity.search = true;
        switch (v.getId()) {
            case R.id.btDeportes:
                for (Articulo a : MainActivity.articulosDB) {
                    if (a.getCategoria().equals("Deportes"))
                        MainActivity.articulosActual.add(a);
                }
                startActivity(new Intent(this, MainActivity.class));
                finish();
                break;
            case R.id.btElectronica:
                for (Articulo a : MainActivity.articulosDB) {
                    if (a.getCategoria().equals("Electronica"))
                        MainActivity.articulosActual.add(a);
                }
                startActivity(new Intent(this, MainActivity.class));
                finish();
                break;
            case R.id.btHogar:
                for (Articulo a : MainActivity.articulosDB) {
                    if (a.getCategoria().equals("Hogar"))
                        MainActivity.articulosActual.add(a);
                }
                startActivity(new Intent(this, MainActivity.class));
                finish();
                break;
            case R.id.btJuguetes:
                for (Articulo a : MainActivity.articulosDB) {
                    if (a.getCategoria().equals("Juguetes"))
                        MainActivity.articulosActual.add(a);
                }
                startActivity(new Intent(this, MainActivity.class));
                finish();
                break;
            case R.id.btLibros:
                for (Articulo a : MainActivity.articulosDB) {
                    if (a.getCategoria().equals("Libros"))
                        MainActivity.articulosActual.add(a);
                }
                startActivity(new Intent(this, MainActivity.class));
                finish();
                break;
            case R.id.btMascotas:
                for (Articulo a : MainActivity.articulosDB) {
                    if (a.getCategoria().equals("Mascotas"))
                        MainActivity.articulosActual.add(a);
                }
                startActivity(new Intent(this, MainActivity.class));
                finish();
                break;
            case R.id.btMotor:
                for (Articulo a : MainActivity.articulosDB) {
                    if (a.getCategoria().equals("Motor"))
                        MainActivity.articulosActual.add(a);
                }
                startActivity(new Intent(this, MainActivity.class));
                finish();
                break;
            case R.id.btRopa:
                for (Articulo a : MainActivity.articulosDB) {
                    if (a.getCategoria().equals("Ropa"))
                        MainActivity.articulosActual.add(a);
                }
                startActivity(new Intent(this, MainActivity.class));
                finish();
                break;
            case R.id.btVideojuegos:
                for (Articulo a : MainActivity.articulosDB) {
                    if (a.getCategoria().equals("Videojuegos"))
                        MainActivity.articulosActual.add(a);
                }
                startActivity(new Intent(this, MainActivity.class));
                finish();
                break;
        }
    }

    public void menuToolbarClick(View v) {
        switch (v.getId()) {
            case R.id.btHome:
                MainActivity.articulosActual.clear();
                MainActivity.search = false;
                startActivity(new Intent(this, MainActivity.class));
                finish();
                break;
            case R.id.btCategories:
                startActivity(new Intent(this, Categorias.class));
                finish();
                break;
            case R.id.btAdd:
                if (MainActivity.actualUser != null) {
                    startActivity(new Intent(this, UploadArticle.class));
                } else {
                    Toast.makeText(this, "Logueate para usar esta funciòn", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(this, Login.class));
                }
                break;
            case R.id.btMsg:
                if (MainActivity.actualUser != null) {
                    startActivity(new Intent(this, Chats.class));
                    finish();
                } else {
                    Toast.makeText(this, "Logueate para usar esta funciòn", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(this, Login.class));
                }
                break;
            case R.id.btProfile:
                if (MainActivity.actualUser != null) {
                    startActivity(new Intent(this, UserProfile.class));
                    finish();
                } else {
                    Toast.makeText(this, "Logueate para usar esta funciòn", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(this, Login.class));
                }
                break;
        }
    }
}