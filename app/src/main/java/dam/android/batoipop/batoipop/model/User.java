package dam.android.batoipop.batoipop.model;


import android.graphics.Bitmap;

public class User {
    private int id;
    private String name, telefono, dni, email, password, direccion, fechanac;
    private Bitmap image;

    public User(int id, String name, String telefono, String dni, String email, String password, String direccion, String fechanac, Bitmap bitmap) {
        this.id = id;
        this.name = name;
        this.telefono = telefono;
        this.dni = dni;
        this.email = email;
        this.password = password;
        this.direccion = direccion;
        this.fechanac = fechanac;
        this.image = bitmap;
    }

    public User(int id, String name, String telefono, String dni, String email, String password, String direccion, String fechanac) {
        this.id = id;
        this.name = name;
        this.telefono = telefono;
        this.dni = dni;
        this.email = email;
        this.password = password;
        this.direccion = direccion;
        this.fechanac = fechanac;
    }

    public User(String name, String telefono, String dni, String email, String password, String direccion, String fechanac) {
        this.name = name;
        this.telefono = telefono;
        this.dni = dni;
        this.email = email;
        this.password = password;
        this.direccion = direccion;
        this.fechanac = fechanac;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getFechanac() {
        return fechanac;
    }

    public void setFechanac(String fechanac) {
        this.fechanac = fechanac;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }
}
